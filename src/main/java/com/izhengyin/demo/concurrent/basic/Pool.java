package com.izhengyin.demo.concurrent.basic;

import com.izhengyin.demo.concurrent.SleepUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author zhengyin
 * Created on 2021/11/30
 */
public class Pool {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Executors.newFixedThreadPool(2);
        Executors.newCachedThreadPool();
        Executors.newScheduledThreadPool(1);
        Future<String> future = executor.submit(() -> {
            SleepUtils.sleep(1000);
            return "xx";
        });
        try {
            String result = future.get();
            System.out.println("result "+result);
            executor.shutdown();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
