package com.izhengyin.demo.concurrent.basic;

import com.izhengyin.demo.concurrent.SleepUtils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @author zhengyin
 * Created on 2021/11/30
 */
public class PoolRelease {
    public static void main(String[] args) {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 20,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(10));
        IntStream.range(0,30)
                        .forEach(i -> {
                            executor.execute(() -> {
                                SleepUtils.sleep(1000);
                                System.out.println(Thread.currentThread().getName());
                            });
                        });
        //jstack 查看线程情况
    }
}
